
const user = {
      name: 'Joe',
      lastName: 'Doe',
      age: 40,
      work: {
            company: 'Google',
            position: 'JS Developer',
            salary: '2000$',
            pos: {
                  a: 1,
                  b: 2
            }
      }
}





function clone(obj) {
      let cloneObj = {}
      for(  let key in obj ) {
           if (typeof obj[key] === 'object') {
                 cloneObj[key] = clone(obj[key])
           } else{
            cloneObj[key] = obj[key]
           }
      }

      return cloneObj
}

let newObj = clone(user)



newObj.name = 'Roman'
newObj.lastName = 'Dunaev'
newObj.age = 29
newObj.work.company = 'Facebook'
newObj.work.position = 'Front-end Developer'
newObj.work.salary= '1000$'
newObj.work.pos.a = 2



console.log(user)
console.log(newObj)



