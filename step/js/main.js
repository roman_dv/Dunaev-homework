const servTabsOpen = () => {
  let  servTabs = document.getElementsByClassName('services-tab');
  servTabs = Array.prototype.slice.call(servTabs)
  let  servItems = document.getElementsByClassName('services-item');
  servItems = Array.prototype.slice.call(servItems)
  let currentShow = 0;

 servTabs.forEach( (el, index)=> {
  el.addEventListener('click', (event)=>{
    if(!el.classList.contains('services-tab-active')) {
      servTabs[currentShow].classList.remove('services-tab-active');
      console.log('first', currentShow)
      currentShow = servTabs.indexOf(event.currentTarget)
      servTabs[currentShow].classList.add('services-tab-active');
      console.log('second', currentShow)
     } 
    servItems.forEach((el2)=>{
        if(el2.classList.contains('grid')) {
          el2.classList.remove('grid')
        }
      })
      servItems.find((el2,index2) => {
        if(index == index2) {
        el2.classList.add('grid')
        }
      })
    })
  });

}
servTabsOpen()


const filterImgs = () => {

  let categoryTabs = document.getElementsByClassName('category-tab');
  categoryTabs = Array.prototype.slice.call(categoryTabs)
  let categoryItems = document.getElementsByClassName('category-item');
  categoryItems = Array.prototype.slice.call(categoryItems)
  let currentShow = 0
  const holder = document.getElementsByClassName('category-items')[0];
  const filterBox = document.createDocumentFragment();
  
  
  categoryTabs.forEach((el)=> {
    el.addEventListener('click', (event)=> {
        if(!el.classList.contains('category-tab-hover')) {
          categoryTabs[currentShow].classList.remove('category-tab-hover');
          currentShow = categoryTabs.indexOf(event.currentTarget)
          categoryTabs[currentShow].classList.add('category-tab-hover');
        };
        if (el.hasAttribute("data-category-all")) {
          categoryItems.forEach((el4)=> {
            filterBox.appendChild(el4)
          });
        } else {
          let elemAttr = el.getAttribute('data-category');
          let filterItems = categoryItems.filter((el2) =>{
              let elemAttr2 = el2.getAttribute('data-category');
              return elemAttr == elemAttr2
            })
            filterItems.forEach((el3)=> {
            filterBox.appendChild(el3)
            })
        }
       
        removeHolder()
        holder.appendChild(filterBox)

      })
  })
  const removeHolder = () => {
      while (holder.firstChild) {
      holder.removeChild(holder.firstChild);
      }
    }

    const addCategoryToSpan = () => {
      categoryItems.forEach((el)=> {
        let span = el.querySelector('.item-hover-category');
        let spanAttr = el.getAttribute('data-category');
        span.innerText = spanAttr
      })
    }
    addCategoryToSpan()
    
    
    const openLoadedImgs = () => {
      let loaded = document.getElementsByClassName('loaded-hide');
      loaded = Array.prototype.slice.call(loaded);
      console.log(loaded)
      
      loaded.forEach((el)=>{
        el.classList.remove('loaded-hide')
      })
      loadImgs.classList.add('loaded-hide')
    }
    
    const loadImgs = document.getElementById('loadMore');
    loadImgs.onclick = openLoadedImgs;
    
}
filterImgs()


const slider = () => {
  const sliderHolder = document.getElementsByClassName("about-us-slides")[0]
  console.log(sliderHolder)
  let slides = document.getElementsByClassName('about-us-slide');
  slides = Array.prototype.slice.call(slides)
  console.log(slides)
  let dots = document.getElementsByClassName('slider-dot');
  dots  = Array.prototype.slice.call(dots)
  let currentShow = 0;

  dots.forEach((el, index) => {
    dots[currentShow].classList.add('slider-dot-active');
    el.addEventListener('click', (event)=>{
      dots[currentShow].classList.remove('slider-dot-active');
      currentShow = dots.indexOf(event.currentTarget)
      dots[currentShow].classList.add('slider-dot-active');

      slides.forEach((el2) => {
        if(el2.classList.contains('current-slide')) {
          el2.classList.remove('current-slide')
        }
      })

      slides.find((el2, index2) => {
        if (index == index2) {
          el2.classList.add('current-slide');
        }
      })
      
    
     })
    })

    const arrowsControl = () => {
      const prev = document.getElementById("prev");
      const next = document.getElementById("next");

      const slideNext = () => {
      
        next.addEventListener("click",  ()=>{
          if(currentShow < 3) {
           dots[currentShow].classList.remove("slider-dot-active")
           slides[currentShow].classList.remove("current-slide")
           dots[++currentShow].classList.add("slider-dot-active")
           slides[currentShow].classList.add("current-slide")
           console.log(currentShow)
          } else if (currentShow == 3){
           dots[currentShow].classList.remove("slider-dot-active");
           slides[currentShow].classList.remove("current-slide")
           currentShow = 0
           dots[currentShow].classList.add("slider-dot-active")
           slides[currentShow].classList.add("current-slide")
          }

          
         
          
          })
       }
       slideNext()

       const slidePrev = () => {
         prev.addEventListener("click",  ()=>{
           if(currentShow != 0) {
          
           console.log(currentShow)
            dots[currentShow].classList.remove("slider-dot-active")
            slides[currentShow].classList.remove("current-slide")
            dots[--currentShow].classList.add("slider-dot-active")
            slides[currentShow].classList.add("current-slide")
             
           } else if (currentShow == 0){
             dots[currentShow].classList.remove("slider-dot-active");
             slides[currentShow].classList.remove("current-slide")
             currentShow = 3
             dots[currentShow].classList.add("slider-dot-active")
             slides[currentShow].classList.add("current-slide")
            }
           })
        }
         slidePrev()

    }

    arrowsControl()

 
}

slider()